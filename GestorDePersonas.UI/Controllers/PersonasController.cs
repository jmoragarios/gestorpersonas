﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestorDePersonas.Model;
using GestorDePersonas.UI.ServicioPersonas;

namespace GestorDePersonas.UI.Controllers
{
    public class PersonasController : Controller
    {
        // GET: Personas
        public ActionResult Index()
        {
            var gestorDePersonas = new ServicioPersonasClient();
            var lista = gestorDePersonas.List();
            return View(lista);
        }

        // GET: Personas/Create

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Personas persona)
        {
            try
            {
                var gestor = new ServicioPersonasClient();

                gestor.Agregar(persona);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {

                return View();
            }
        }

        [HttpGet]
        public ActionResult Casarse(int id)
        {
            var gestorDePersonas = new ServicioPersonasClient();
            var casarViewModel = gestorDePersonas.ListarPersonasDisponibles(id);
            casarViewModel.IdPersona = id;
            


            return View(casarViewModel);
        }

        
        public ActionResult Casar(int id, int idPersona)
        {
            //var str = id.Split('-');
            //var IdSeleccionado = int.Parse(str[0]);
            //var IdModel = int.Parse(str[1]);
            var operacion = new ServicioPersonasClient();
            var personaA = operacion.Consultar(id);
            var personaB = operacion.Consultar(idPersona);
            personaA.IdPareja = idPersona;
            personaB.IdPareja = id;
            operacion.Actualizar(personaA);
            operacion.Actualizar(personaB);

            return RedirectToAction("Index");
        }

        // GET: Personas/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Personas/Create
        public ActionResult Edit(int id)
        {
            var operacion = new ServicioPersonasClient();
            var orden = operacion.Consultar(id);

            return View(orden);
        }

        // POST: Personas/Edit/5
        [HttpPost]
        public ActionResult Edit(Personas personas)
        {
            try
            {
                // TODO: Add update logic here
                var operacion = new ServicioPersonasClient();
                operacion.Actualizar(personas);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        // GET: Personas/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Personas/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
