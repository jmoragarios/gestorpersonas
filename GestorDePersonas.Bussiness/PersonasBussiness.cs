﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestorDePersonas.Model;
using GestorDePersonas.DataAcces;

namespace GestorDePersonas.Bussiness
{
   public class PersonasBussiness
    {
        public List<Personas> List()
        {
            var personDataAccess = new OperacionesPersonas();
            var result = personDataAccess.List();
            foreach (var item in result)
            {
                item.SexoEnum = item.Sexo ? SexoEnum.Mujer : SexoEnum.Hombre;
            }
            var today = DateTime.Today;
            foreach (var item in result)
            {

                var age = today.Year - item.FechaDeNacimiento.Year;

                if (item.FechaDeNacimiento.Date > today.AddYears(-age)) age--;
                item.Edad = age;
            }

            return result;
        }

        public void Agregar(Personas persona)
        {

            var operaciones = new OperacionesPersonas();
            persona.Sexo = persona.SexoEnum == SexoEnum.Mujer ? true : false;
            operaciones.AgregarPersona(persona);
        }

        public Personas Consultar(int Id)
        {
            DataAcces.OperacionesPersonas operaciones;
            operaciones = new DataAcces.OperacionesPersonas();

            return operaciones.ObtenerPorId(Id);
        }

        public void Actualizar(Personas personas)
        {

            DataAcces.OperacionesPersonas operaciones;
            operaciones = new DataAcces.OperacionesPersonas();
            var consulta = Consultar(personas.Id);

            personas.Id = consulta.Id;
            personas.FechaDeNacimiento = consulta.FechaDeNacimiento;
            

            operaciones.ActualizarPersona(personas);

        }

        public CasarViewModel ListarPersonasDisponibles(int id)
        {
            var operaciones = new OperacionesPersonas();
            var resultado = new CasarViewModel();
            var personasDisponibles = operaciones.ListarPersonasDisponibles(id);
            var today = DateTime.Today;
            foreach (var item in personasDisponibles)
            {
                
                var age = today.Year - item.FechaDeNacimiento.Year;
                
                if (item.FechaDeNacimiento.Date > today.AddYears(-age)) age--;
                item.Edad = age;
            }

            resultado.PersonasDisponibles = personasDisponibles.Where(x => x.Edad >= 18).ToList();

            return resultado;
        }

    }
}
