﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDePersonas.Model
{
    public enum SexoEnum
    {
        Hombre = 0,
        Mujer = 1
    }
}
