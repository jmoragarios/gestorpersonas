﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDePersonas.Model
{
    public class CasarViewModel
    {
        public int IdPersona { get; set; }
        public List<Personas> PersonasDisponibles { get; set; }
    }
}
