﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDePersonas.Model
{
   public class Personas
    {
        public int      Id                  { get; set; }
        [Required(ErrorMessage = "El campo Nombre es requerido")]
        public string   Nombre              { get; set; }
        [Required(ErrorMessage = "El campo Primer Apellido es requerido")]
        [Display(Name = "Primer Apellido")]
        public string   PrimerApellido      { get; set; }
        [Required(ErrorMessage = "El campo Segundo Apellido es requerido")]
        [Display(Name = "Segundo Apellido")]
        public string   SegundoApellido     { get; set; }
        [Required(ErrorMessage = "El campo Fecha es requerido")]
        [DataType(DataType.Date, ErrorMessage = "El campo Fecha de nacimiento debe ser del tipo fecha")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Fecha de nacimiento")]
        public DateTime FechaDeNacimiento   { get; set; }

        public bool Sexo { get; set; }
        public int? IdPadre { get; set; }
        public int? IdMadre { get; set; }
        public int? IdPareja { get; set; }

        [NotMapped]
        public int      Edad                { get; set; }
        [NotMapped]
        [Display(Name = "Nombre del Padre")]
        public string   NombreDelPadre      { get; set; }
        [NotMapped]
        [Display(Name = "Nombre de la Madre")]
        public string   NombreDeLaMadre     { get; set; }

        [NotMapped]
        [Display(Name = "Cantidad de hijos")]
        public int CantidadDeHijos { get; set; }
        [NotMapped]
        [Display(Name = "Estado Civil")]
        public String EstadoCivil { get; set; }
        [NotMapped]
        [Display(Name = "Sexo")]
        public SexoEnum SexoEnum { get; set; }

    }
}
