﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using GestorDePersonas.Model;
using GestorDePersonas.Bussiness;

namespace GestorDePersonas.SI
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ServicioPersonas : IServicioPersonas
    {
        public List<Personas> List()
        {
            PersonasBussiness personBusiness = new PersonasBussiness();

            return personBusiness.List();
        }

        public void Agregar(Personas persona)
        {
            PersonasBussiness personaBusiness = new PersonasBussiness();
            personaBusiness.Agregar(persona);

        }

        public Personas Consultar(int Id)
        {
            PersonasBussiness personasBussiness = new PersonasBussiness();
            return personasBussiness.Consultar(Id);
        }

        public void Actualizar(Personas personas)
        {
            PersonasBussiness personasBussiness = new PersonasBussiness();
            personasBussiness.Actualizar(personas);
        }

        public CasarViewModel ListarPersonasDisponibles(int id)
        {
            var personasBusiness = new PersonasBussiness();
            return personasBusiness.ListarPersonasDisponibles(id);
        }

    }
}
