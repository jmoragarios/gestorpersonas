﻿using GestorDePersonas.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GestorDePersonas.SI
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IServicioPersonas
    {

        [OperationContract]
        List<Model.Personas> List();

        [OperationContract]
        void Agregar(Personas persona);

        [OperationContract]
        Personas Consultar(int Id);

        [OperationContract]
        void Actualizar(Personas personas);

        [OperationContract]
        CasarViewModel ListarPersonasDisponibles(int id);

        // TODO: agregue aquí sus operaciones de servicio
    }
}
