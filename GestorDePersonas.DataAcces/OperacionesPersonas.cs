﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestorDePersonas.Model;
using System.Data.Entity;

namespace GestorDePersonas.DataAcces
{
   public class OperacionesPersonas
    {
        public List<Personas> List()
        {
            var db = new Context();
            var result = db.Personas.ToList();

            return result;
        }

        public void AgregarPersona(Personas persona)
        {
            var db = new Context();

            db.Personas.Add(persona);
            db.Entry(persona).State = EntityState.Added;
            db.SaveChanges();
        }

 
        public List<Personas> ListarPersonasDisponibles(int id)
        {
            var db = new Context();
            var persona = ObtenerPorId(id);
            var listaPersonas = List();
            var validacioSolteriaYSexo = listaPersonas.Where(x => x.IdPareja == null && x.Sexo != persona.Sexo).ToList();
            var validacionHermandad = validacioSolteriaYSexo.Where(x => (x.IdPadre == null && x.IdMadre == null) || (x.IdPadre != persona.IdPadre && x.IdMadre != persona.IdMadre)).ToList();
            var listaIdHermanos = (from x in listaPersonas where x.IdPadre == persona.IdPadre && x.IdMadre == persona.IdMadre select x.Id).ToList();
            var validacionSobrinos = validacionHermandad.Where(x => (x.IdMadre != null && !listaIdHermanos.Contains(x.IdMadre.Value)) && (x.IdPadre != null && !listaIdHermanos.Contains(x.IdPadre.Value))).ToList();
            var resultado = validacionSobrinos;
            foreach (var item in listaPersonas)
            {
                if (item.IdMadre == null && item.IdPadre == null && item.Sexo != persona.Sexo && item.IdPareja == null )

                    resultado.Add(item);
            }

            return resultado;
        }

        public Personas ObtenerPorId(int Id)
        {
            var db = new Context();
            var result = db.Personas.Find(Id);

            return result;
        }

        public void ActualizarPersona(Personas actualizarPersona)
        {
            var db = new Context();
            var personaOriginal = ObtenerPorId(actualizarPersona.Id);

            personaOriginal.Nombre = actualizarPersona.Nombre;
            personaOriginal.PrimerApellido = actualizarPersona.PrimerApellido;
            personaOriginal.SegundoApellido = actualizarPersona.SegundoApellido;
            personaOriginal.FechaDeNacimiento = actualizarPersona.FechaDeNacimiento;
            personaOriginal.IdPareja = actualizarPersona.IdPareja;

            db.Entry(personaOriginal).State = EntityState.Modified;
            db.SaveChanges();
        }



    }
}
