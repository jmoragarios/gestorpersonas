﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using GestorDePersonas.Model;

namespace GestorDePersonas.DataAcces
{
    class Context: DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Personas>().ToTable("Personas");
        }

        public DbSet<Personas> Personas { get; set; }
    }
}
